// Простой перебор страниц одна за одной. Категория выбирается без дополнительных параметров
var request = require('request');
var urlapi = 'http://sima-land.ru/api/v2/';

function GetAllCategoryPage(url, numberpage) {
    if (url != 'end') {
        var UrlCategoryPage = url + "item?page=" + numberpage;
        request(UrlCategoryPage, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var obj = JSON.parse(body);
                //console.log(obj);
                //Если есть следующая страница
                if (obj._links.next) {
                    var num = parseInt(obj._links.next.href.replace(/https:\/\/127.0.0.1:9100\/api\/v2\/item\?page=/gi, ''));
                    console.log(UrlCategoryPage);
                    GetAllCategoryPage(urlapi, num);
                };
            } else {
                console.log("ERROR from url->" + UrlCategoryPage);
                console.log(body);
            };
        });
    };
};
GetAllCategoryPage(urlapi, 1);