// Асинхронный перебор страниц. Категория выбирается без дополнительных параметров
// Здесь выставляются ограничение по количеству запросов по времени
// первый параметр количество запросов, второй за какое время (милесикунды)
var limit = require("simple-rate-limiter");
var request = limit(require("request")).to(250).per(10000);
var urlapi = 'http://sima-land.ru/api/v2/';

function GetCategoryPage(url, numberpage) {
    if (url != 'end') {
        var UrlCategoryPage = url + "category?page=" + numberpage;
        request(UrlCategoryPage, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var obj = JSON.parse(body);
                console.log("OK url->"+UrlCategoryPage);
            } else {
                console.log("ERROR from url->" + UrlCategoryPage);
                console.log(body);
            };
        });
    };
};

function GetAllCategoryPageSpeed(url) {
    var UrlCategoryPage = url + "category?page=" + 1;
    request(UrlCategoryPage, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var obj = JSON.parse(body);
            var end = parseInt(obj._links.last.href.replace(/https:\/\/127.0.0.1:9100\/api\/v2\/category\?page=/gi, ''));
            for (var i = 1; i < end; i++) {
                //console.log(i);
                GetCategoryPage(urlapi, i);
            };
        } else {
            console.log("ERROR from url->" + UrlCategoryPage);
            console.log(body);
        };
    });
};
GetAllCategoryPageSpeed(urlapi);